FROM debian:bookworm

ARG HADOLINT_VERSION 2.12.0

ENV HADOLINT_VERSION ${HADOLINT_VERSION}

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install wget && \
    wget "https://github.com/hadolint/hadolint/releases/download/v"${HADOLINT_VERSION}"/hadolint-Linux-x86_64" && \
    mv hadolint-Linux-x86_64 /bin/hadolint && \
    chmod +x /bin/hadolint